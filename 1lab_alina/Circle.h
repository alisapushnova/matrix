#include <vcl.h>
#include <vector>


class Circle
	{
	public :
		Circle();
		double Xcentr;
		double Ycentr;
		std::vector <class Figura*> List;
		double Rad;
		Circle(double x, double y, double R);
		double Perimetr();
		double Plos();
        double getRad();
		void Show(TCanvas *Canvas, TColor Color);
		void Mashtabvel(double Rd, double t);
		void Mashtabmin(double Rd, double t);
		void Mashtabmaxtochka(double Rd, double t, int marker);
		void Mashtabmintochka(double Rd, double t, int marker);
        void vrash(double x, double y, double ygol, double rad);
		int proverka(double XX, double YY);
		double rast(double XX, double YY);
		void Move(int x, int y);
		double centrX();
		double centrY();
	}  ;
