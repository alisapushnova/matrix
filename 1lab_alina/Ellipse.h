#include <vcl.h>
#include <vector>
#include "Circle.h"

class Ellips : public Circle{
	  public :
		Ellips(double x, double y);
		double a;
		double b;
		double geta();
        double getb();
		void dobav(double x, double y, double XX, double YY);
		double Plosh();
		double dlina();
		void Mashtabmintochkael(double a, double b, double t, int marker);
		void Mashtabmaxtochkael(double a, double b, double t, int marker);
		void Mashtabvelel(double a,double b, double t);
		void Mashtabminel(double a,double b, double t);
		void vrashs(double yx, double yy, double ygol, double a, double b);
		Ellips();
	}  ;
