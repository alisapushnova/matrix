#include "Ellipse.h"
#include "figura.h"


		Ellips :: Ellips(){
		List.clear();
		}

		Ellips::Ellips(double x, double y)
		{
		Figura(x,y);
		}

		void Ellips::dobav(double x, double y, double XX, double YY)
		{
		List.clear();
		List.push_back(new Figura(x+(XX-x)/2, y+(YY-y)/2));
		List.push_back(new Figura(x, y));
		List.push_back(new Figura(XX, YY));
		}

			double Ellips::Plosh()
		{
			double S;
			double a= (List[0]->x-List[1]->x);
			double b= (List[0]->y-List[1]->y);
			S=3.14*a*b;
			return S;
		}


			double Ellips::dlina()
		{
			double P;
			double a= (List[0]->x-List[1]->x);
			double b= (List[0]->y-List[1]->y);
			double n = 3*a+b;
			double m=a+3*b;
			double r=sqrt(n*m);
			P=3.14*(3*(a+b)-r);
			return P;
		}

		void Ellips::Mashtabmaxtochkael(double a,double b, double t, int marker){
		double Shaga=a*(t-1)/100;
		double Shagb=b*(t-1)/100;
		switch(marker){
		case 0:{
		List[2]->x+=2*Shaga;
		List[2]->y+=2*Shagb;
		break;
		}
		case 1:{
		List[1]->x-=2*Shaga;
		List[2]->y+=2*Shagb;
		break;
		}
		case 2:{
		List[1]->x-=2*Shaga;
		List[1]->y-=2*Shagb;
		break;
		}
		case 3:{
		List[1]->y-=2*Shaga;
		List[2]->x+=2*Shagb;
		break;
		}
		}
		}

		void Ellips::Mashtabmintochkael(double a, double b, double t, int marker){
		double Shaga=a*(1-t)/100;
		double Shagb=b*(1-t)/100;
		switch(marker){
		case 0:{
		List[2]->x-=2*Shaga;
		List[2]->y-=2*Shagb;
		break;
		}
		case 1:{
		List[1]->x+=2*Shaga;
		List[2]->y-=2*Shagb;
		break;
		}
		case 2:{
		List[1]->x+=2*Shaga;
		List[1]->y+=2*Shagb;
		break;
		}
		case 3:{
		List[1]->y+=2*Shaga;
		List[2]->x-=2*Shagb;
		break;
		}
		}
		}

			void Ellips::Mashtabvelel(double a,double b, double t){
		double Shaga=a*(t-1)/100;
		double Shagb=b*(t-1)/100;
		List[1]->x-=Shaga;
		List[1]->y-=Shagb;
		List[2]->x+=Shaga;
		List[2]->y+=Shagb;
		}

			void Ellips::Mashtabminel(double a, double b, double t){

		double Shaga=a*(1-t)/100;
		double Shagb=b*(1-t)/100;
		List[1]->x+=Shaga;
		List[1]->y+=Shagb;
		List[2]->x-=Shaga;
		List[2]->y-=Shagb;
		}

		double	 Ellips:: geta(){
		double a= List[0]->x-List[1]->x;
		return a;
		}

		 double	Ellips:: getb(){
		double b= (List[0]->y-List[1]->y);
		return b;
		}

		void Ellips::vrashs(double yx, double yy, double ygol, double a, double b)   {

		List[1]->x=(List[1]->x-yx)*cos(ygol)-(List[1]->y-yy)*sin(ygol)+yx;
		List[1]->y=(List[1]->x-yx)*sin(ygol)+(List[1]->y-yy)*cos(ygol)+yy;
		List[0]->x=List[1]->x+a;
		List[0]->y=List[1]->y+b;
		List[2]->x=List[1]->x+2*a;
		List[2]->y=List[1]->y+2*b;
		}
