#include "Circle.h"
#include "figura.h"
#include "math.h"

		Circle::Circle(){
		List.clear();
		}

		Circle::Circle(double x, double y, double R)
		{
		List.clear();
		List.push_back(new Figura(x, y));
		Rad=R;
		List.push_back(new Figura(x-R, y-R));
		List.push_back(new Figura(x+R, y+R));
		}

		double Circle::Perimetr()
		{
			double P;
			P=3.14*2*Rad;
			return P;
		}

			double Circle::getRad()
		{
			double R=List[2]->x-List[1]->x;
			return R/2;
		}

		double Circle::Plos()
		{
			double S;
			S=3.14*powl(Rad,2);
			return S;
		}

		void Circle::Show(TCanvas *Canvas, TColor Color)
		{
		int X1=400;
		int Y1=100;
		Canvas->Pen->Color=Color;
		Canvas->Ellipse(List[1]->x+X1,List[1]->y+Y1,List[2]->x+X1,List[2]->y+Y1);
		}




		void Circle::Mashtabvel(double Rd, double t){
		double Shag=Rd*(t-1)/100;
		List[1]->x-=Shag;
		List[1]->y-=Shag;
		List[2]->x+=Shag;
		List[2]->y+=Shag;
		}

			void Circle::Mashtabmin(double Rd, double t){

		double Shag=Rd*(1-t)/100;
		List[1]->x+=Shag;
		List[1]->y+=Shag;
		List[2]->x-=Shag;
		List[2]->y-=Shag;
		}

		void Circle::Mashtabmintochka(double Rd, double t, int marker){
		double Shag=Rd*(1-t)/100;
		switch(marker){
		case '0':{
		List[2]->x-=2*Shag;
		List[2]->y-=2*Shag;
		break;
		}
		case '1':{
		List[1]->x+=2*Shag;
		List[2]->y-=2*Shag;
		break;
		}
		case '2':{
		List[1]->x+=2*Shag;
		List[1]->y+=2*Shag;
		break;
		}
		case '3':{
		List[1]->y+=2*Shag;
		List[2]->x-=2*Shag;
		break;
		}
		}
		}

		double Circle::rast(double XX, double YY) {
		double r=powl((powl(List[0]->x-XX,2)+powl(List[0]->y-YY,2)),(1/2));
		return r;
		}

		int Circle::proverka(double XX, double YY) {
		int marker;
		if(List[0]->x>=XX&&List[0]->y>=YY)marker=0;
		if(List[0]->x<=XX&&List[0]->y>=YY)marker=1;
		if(List[0]->x>=XX&&List[0]->y<=YY)marker=2;
		if(List[0]->x>=XX&&List[0]->y<=YY)marker=3;
		return marker;
		}

			void Circle::Mashtabmaxtochka(double Rd, double t, int marker){
		double Shag=Rd*(t-1)/100;
		switch(marker){
		case '0':{
		List[2]->x+=2*Shag;
		List[2]->y+=2*Shag;
		break;
		}
		case '1':{
		List[1]->x-=2*Shag;
		List[2]->y+=2*Shag;
		break;
		}
		case '2':{
		List[1]->x-=2*Shag;
		List[1]->y-=2*Shag;
		break;
		}
		case '3':{
		List[1]->y-=2*Shag;
		List[2]->x+=2*Shag;
		break;
		}
		}
		}


			void Circle::Move(int x, int y){
		   for(int i=0;i<3;i++){
			 List[i]->x+=x;
			 List[i]->y+=y;
		   }
		}


		double Circle::centrX(){
			return List[0]->x;
		}


		double Circle::centrY(){
			return List[0]->y;
		}

		void Circle::vrash(double yx, double yy, double ygol, double rad)   {

		List[1]->x=(List[1]->x-yx)*cos(ygol)-(List[1]->y-yy)*sin(ygol)+yx;
		List[1]->y=(List[1]->x-yx)*sin(ygol)+(List[1]->y-yy)*cos(ygol)+yy;
		List[0]->x=List[1]->x+rad;
		List[0]->y=List[1]->y+rad;
		List[2]->x=List[1]->x+2*rad;
		List[2]->y=List[1]->y+2*rad;
		}
