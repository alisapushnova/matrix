//
//  main.cpp
//  ex2
//
//  Created by Пушнова Алиса Сергеевна on 11/4/18.
//  Copyright © 2018 Пушнова Алиса Сергеевна. All rights reserved.
//

#include <iostream>
#include <math.h>
#include <time.h>
#include <stdlib.h>

using namespace std;

int amount_of_elements_array(double array[], int one_element, int N)
{
    if (N == one_element){
        if (sin(array[N] / 2) < 0)
            return 1;
        return 0;
    }
    else return amount_of_elements_array(array, one_element, (N + one_element) / 2) +
        amount_of_elements_array(array, (N + one_element) / 2 + 1, N);
}

int main()
{
    // srand (time(NULL));
    
    int N;
    cin >> N;
    
    double *array = (double*)malloc(N * sizeof(double));
    
    for (int i = 0; i < N; i++)
    {
        array[i] = rand() % 10 + 1;
        cout << array[i] << " ";
    }
    
    cout << "\n" << amount_of_elements_array(array, 0, N);
    
    return 0;
}


