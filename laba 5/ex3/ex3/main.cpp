//
//  main.cpp
//  ex3
//
//  Created by Пушнова Алиса Сергеевна on 11/7/18.
//  Copyright © 2018 Пушнова Алиса Сергеевна. All rights reserved.
//
#include <iostream>
#include <math.h>
using namespace std;


double function1(double a, double b, double n)
{
    double h = (b - a) / n;
    
    double amount = 0;
    
    for (int i = 0; i < n - 1; i++)
    {
        double xi = a + (i * h) + (h / 2);
    
        double function1 = sqrt(1.5 * xi + 0.6) / (1.6 + sqrt(0.8 * powl(xi, 2) + 2));
        
        amount += function1 * (xi);
    
    }
    double integral = h * amount;
    cout << "Интеграл 1 равен = " << integral;
    
    return amount;
}

double function2(double a, double b, double n)
{
    double h = (b - a) / n;
    
    double amount = 0;
    
    
    for (int i = 0; i < n - 1; i++)
    {
        double xi = a + (i * h) + (h / 2);
        
        double function2 = cos(0.6 * powl(xi, 2) + 0.4) / (1.4 + powl(sin(xi + 0.7), 2));
        
        amount += function2 * (xi);
    }
    double integral = h * amount;
    cout << "Интеграл 2 равен = " << integral;
    
    return amount;
}

double function3(double a, double b, double n)
{
    double h = (b - a) / n;
    
    double amount = 0;
    
    for (int i = 0; i < n - 1; i++)
    {
        double xi = a + (i * h) + (h / 2);
        
        double function3 = 1.0 / sqrt(powl(xi, 2) + 1);
        
        amount += function3 * (xi); // function1(xi);
    }
    double integral = h * amount;
    cout << "Интеграл 3 равен = " << integral;
    
    return amount;
}

double function4(double a, double b, double n)
{
    double h = (b - a) / n;
    
    double amount = 0;
    
    for (int i = 0; i < n - 1; i++)
    {
        double xi = a + (i * h) + (h / 2);
        
        double function4 = cos(xi) / (xi + 1);
        
        amount += function4 * (xi); // function1(xi);
    }
    double integral = h * amount;
    cout << "Интеграл 4 равен = " << integral;
    
    return amount;
}

int main()
{
    int number_of_integral = 0;
    cout << "Введите интеграл, который хотитет прсчитать: ";
    cin >> number_of_integral;
    
    switch (number_of_integral)
    {
        case 1: function1(1.0, 2.2, 100);
            break;
        case 2: function2(0.6, 1.0, 100);
            break;
        case 3: function3(0.2, 1.2, 100);
            break;
        case 4: function4(0.6, 1.4, 100);
            break;
    }
    
    return 0;
}

