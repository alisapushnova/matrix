//
//  main.cpp
//  ex1
//
//  Created by Пушнова Алиса Сергеевна on 12/15/18.
//  Copyright © 2018 Пушнова Алиса Сергеевна. All rights reserved.
//

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class reference_bus_station
{
public:
    long date;
    string bus_number;
    string bus_type;
    string destination;
    string time_of_departure;
    string time_of_arrival;
};

reference_bus_station add_infornation()
{
    system("cls"); // clean console
    long date;
    string bus_number;
    string bus_type;
    string destination;
    string time_of_departure;
    string time_of_arrival;
    
    cout << "\t\n" << string(63, '=').c_str();
    cout << "\t\n" << "Номер автобуса:          | ";
    cout << "\t\n" << "Тип автобуса:            | ";
    cout << "\t\n" << "Пункт назначения:        | ";
    cout << "\t\n" << "Время отправления:       | ";
    cout << "\t\n" << "Время прибытия:          | ";
    cout << "\t\n" << string(63, '=').c_str();
    
    cin >> date;
    
    char tmp[101];
    
    // Everywhere I need to assign "variable =  tmp;"?
    
    cin.getline (tmp, 100);
    bus_number =  tmp;
    
    cin.getline (tmp, 100);
    bus_type = tmp;
    
    cin.getline (tmp, 100);
    destination = tmp;
    
    cin.getline (tmp, 100);
    time_of_departure = tmp;
    
    cin.getline (tmp, 100);
    time_of_arrival = tmp;
    
    return { date, bus_number, bus_type, destination, time_of_departure, time_of_arrival };
}

void Print_Entry(reference_bus_station &entry)
{
    cout << "\t" << "Дата:                    "  << entry.date << "\n";
    cout << "\t" << "Номер автобуса:          "  << entry.bus_number << "\n";
    cout << "\t" << "Тип автобуса:            "  << entry.bus_type << "\n";
    cout << "\t" << "Пункт назначения:        "  << entry.destination << "\n";
    cout << "\t" << "Время отправления:       "  << entry.time_of_departure << "\n";
    cout << "\t" << "Время прибытия:          "  << entry.time_of_arrival << "\n";
}

void Print_Reference_List(vector<reference_bus_station> &info)
{
    system("cls");
    if (info.size() > 0)
    {
        for (unsigned int i = 0; i < info.size(); i++)
        {
            cout << string(63, '=').c_str();
            cout << "Входные данные " << i + 1 << "\n";
            Print_Entry(info[i]);
        }
        cout << string(63, '=').c_str();
    }
    else
    {
        cout << "Нет данных для вывода!\nНажмите любую клавишу для выхода в главное меню...\n";
    }
    getchar();
}

void Menu_Dialog()
{
    system("cls");
    
    cout << "\t\n" << string(63, '-').c_str();
    cout << "\t\n" << "|                   Что вы хотитете сделать?                  |";
    cout << "\t\n" << string(63, '-').c_str();
    cout << "\t\n" << "| 1 | Выдать всю информацию о рейсах:                         |";
    cout << "\t\n" << "| 2 | Выдать рейсы, которые придут до указанного времени:     |";
    cout << "\t\n" << "| 3 | Добавить информацию о рейсах:                           |";
    cout << "\t\n" << "| 4 | Выход из программы:                                     |";
    cout << "\t\n" << string(60, '-').c_str();
    
}

void Sort_Data(vector<reference_bus_station> &data)
{
    for (unsigned int i = 0; i < data.size(); i++)
        for (unsigned int j = 0; j < data.size() - i - 1; j++)
            if (data[j].date < data[j + 1].date) swap(data[j], data[j + 1]);
}

void Edit_Dialog(reference_bus_station &entry)
{
    system("cls"); // clean console
    long date;
    string bus_number;
    string bus_type;
    string destination;
    string time_of_departure;
    string time_of_arrival;
    
    cout << "\t\n" << string(63, '=').c_str();
    cout << "\t\n" << "Дата:                    | ";
    cout << "\t\n" << "Номер автобуса:          | ";
    cout << "\t\n" << "Тип автобуса:            | ";
    cout << "\t\n" << "Пункт назначения:        | ";
    cout << "\t\n" << "Время отправления:       | ";
    cout << "\t\n" << "Время прибытия:          | ";
    cout << "\t\n" << string(63, '=').c_str();
    
    cin >> date;
    
    char tmp[101];
    
    cin.getline (tmp, 100);
    bus_number = tmp;
    
    cin.getline (tmp, 100);
    bus_type = tmp;
    
    cin.getline (tmp, 100);
    destination = tmp;
    
    cin.getline (tmp, 100);
    time_of_departure = tmp;
    
    cin.getline (tmp, 100);
    time_of_arrival = tmp;
    
    entry.date = date;
    entry.bus_number = bus_number;
    entry.bus_type = bus_type;
    entry.destination = destination;
    entry.time_of_departure = time_of_departure;
    entry.time_of_arrival = time_of_arrival;
}

void Find_Entry(vector<reference_bus_station> &data)
{
    system("cls");
    
    char tmp[101];
    string number;
    cout << "Введите номер рейса: ";
    cin.getline(tmp, 100);
    number = tmp;
    
    system("cls");
    
    vector<int> indexes = vector<int>();
    for (unsigned int i = 0; i < data.size(); i++)
    {
        if (data[i].bus_number == number) indexes.push_back(i);
    }
    
    if (!indexes.empty())
    {
        int chosse;
        
        for (unsigned int i = 0; i < indexes.size(); i++)
        {
            cout << string(63, '=').c_str();
            cout << "Входные данные " << indexes[i] + 1 << "\n";
            Print_Entry(data[indexes[i]]);
        }
        cout << string(63, '=').c_str();
        
        if (indexes.size() > 1)
        {
            do
            {
                cout << "Какой конкретно рейс Вы хотите посмотреть?: ";
                cin >> chosse;
                chosse--;
            }
            while (find(indexes.begin(), indexes.end(), chosse) == indexes.end());
            //system("cls");
        }
        else
        {
           chosse = indexes[0];
        }
        
        cout << "\n Что вы хотите сделать?\n";
        cout << "\t 1 - Изменить запись\n";
        cout << "\t 2 - Удалить запись\n";
        cout << "\t 3 - Выйти в главное меню\n";
        
        bool quit = false;
        while (!quit)
        {
            switch (getchar())
            {
                case '1':
                    Edit_Dialog(data[chosse]);
                    quit = true;
                    break;
                case '2':
                    data.erase(data.begin() + chosse);
                    quit = true;
                    break;
                case '3':
                    quit = true;
                    break;
            }
        }
    }
    else
    {
        cout << "Нет записей с подходящими свойствами!\nНажмите любую клавишу, чтобы вернуться в главное меню...\n";
        getchar();
    }
}
    
    
int main()
{
    auto info = vector<reference_bus_station>();
    
    bool quit = false;
    while (!quit)
    {
        Menu_Dialog();
        switch (getchar())
        {
            case '1':
                Print_Reference_List(info);
                break;
            case '2':
                info.push_back(reference_bus_station());
                break;
            case '3':
                Sort_Data(info);
                break;
            case '4':
                Find_Entry(info);
                break;
            case '5':
                quit = true;
                break;
        }
        system("cls");
    }
    return 0;
}
